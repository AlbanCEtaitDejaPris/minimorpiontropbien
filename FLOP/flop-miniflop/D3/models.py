# -*- coding: utf-8 -*-

# This file is part of the MiniFlOp project.
# Copyright (c) 2019
# Authors: Iulian Ober, Paul Renaud-Goud, Pablo Seban, et al.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <http://www.gnu.org/licenses/>.

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from enum import Enum

# <editor-fold desc="GROUPS">
# ------------
# -- GROUPS --
# ------------

class Group(models.Model):
    name = models.CharField(max_length=4)
    parent_group = models.ForeignKey('self',
                                     blank=True,
                                     null=True,
                                     related_name="children_group",
                                     on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def ancestor_groups(self):
        """
        :return: the set of all Group containing self (self not included)
        """
        all = set()

        if self.parent_group:
            all.add(self.parent_group)
            all|=self.parent_group.ancestor_groups()

        return all


# </editor-fold desc="GROUPS">

# <editor-fold desc="TIMING">
# ------------
# -- TIMING --
# ------------

# will be used only for constants
# TO BE CLEANED at the end (fields and ForeignKeys)
class Day(Enum):
    MONDAY = "m"
    TUESDAY = "tu"
    WEDNESDAY = "w"
    THURSDAY = "th"
    FRIDAY = "f"
    SATURDAY = "sa"
    SUNDAY = "su"

# </editor-fold>

# <editor-fold desc="ROOMS">
# -----------
# -- ROOMS --
# -----------


class RoomType(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(max_length=20)
    room_type = models.ManyToManyField(RoomType,
                                   blank=True,
                                   related_name="members")

    def __str__(self):
        return self.name

# </editor-fold>

# <editor-fold desc="COURSES">
# -------------
# -- COURSES --
# -------------


class Module(models.Model):
    name = models.CharField(max_length=100, null=True)
    abbrev = models.CharField(max_length=10, verbose_name='Intitulé abbrégé')
    head = models.ForeignKey('people.Tutor',
                             null=True,
                             default=None,
                             blank=True,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.abbrev

    class Meta:
        ordering = ['abbrev', ]


class Course(models.Model):
    room_type = models.ForeignKey('RoomType', null=True, on_delete=models.CASCADE)
    tutor = models.ForeignKey('people.Tutor',
                              related_name='taught_courses',
                              null=True,
                              default=None,
                              on_delete=models.CASCADE)

    group = models.ForeignKey('Group', on_delete=models.CASCADE)
    module = models.ForeignKey('Module', related_name='module', on_delete=models.CASCADE)
    duration = models.PositiveSmallIntegerField(default=90)
    week = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(53)],
        null=True, blank=True)
    year = models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.module}-{self.tutor.username}-{self.group}"


class ScheduledCourse(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    day = models.CharField(max_length=2, default=Day.MONDAY,
                           choices=[(d, d.value) for d in Day])
    # in minutes from 12AM
    start_time = models.PositiveSmallIntegerField()
    room = models.ForeignKey('Room', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.course}{self.day}-t{self.start_time}-{self.room}"


# </editor-fold desc="COURSES">


# <editor-fold desc="MODIFICATIONS">
# -----------------
# - MODIFICATIONS -
# -----------------


# null iff no change
class CourseModification(models.Model):
    course = models.ForeignKey('Course', on_delete=models.CASCADE)
    week_old = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(53)], null=True)
    year_old = models.PositiveSmallIntegerField(null=True)
    room_old = models.ForeignKey('Room', blank=True, null=True, on_delete=models.CASCADE)
    day_old = models.CharField(max_length=2,
                               choices=[(d, d.value) for d in Day], default=None, null=True)
    start_time_old = models.PositiveSmallIntegerField(default=None, null=True)
    version_old = models.PositiveIntegerField()
    updated_at = models.DateTimeField(auto_now=True)
    initiator = models.ForeignKey('people.Tutor', on_delete=models.CASCADE)

    def __str__(self):
        olds = 'OLD:'
        if self.week_old is not None:
            olds += f' Sem {self.week_old} ;'
        if self.year_old is not None:
            olds += f' An {self.year_old} ;'
        if self.room_old is not None:
            olds += f' Salle {self.room_old} ;'
        if self.day_old is not None:
            olds += f' Cren {self.day_old}-{self.start_time_old} ;'
        if self.version_old is not None:
            olds += f' NumV {self.version_old} ;'
        return f"by {self.initiator.username}, at {self.updated_at}\n" + \
               f"{self.course} <- {olds}"


# </editor-fold desc="MODIFICATIONS">


# <editor-fold desc="DISPLAY">
# -------------
# -- DISPLAY --
# -------------


class ModuleDisplay(models.Model):
    module = models.OneToOneField('Module', related_name='display',
                                  on_delete=models.CASCADE)
    color_bg = models.CharField(max_length=20, default="red")
    color_txt = models.CharField(max_length=20, default="black")

    def __str__(self):
        return f"{self.module} -> BG: {self.color_bg} ; TXT: {self.color_txt}"


class GroupDisplay(models.Model):
    group = models.OneToOneField('Group',
                                 related_name='display',
                                 on_delete=models.CASCADE)
    button_height = models.PositiveIntegerField(null=True, default=None)
    button_txt = models.CharField(max_length=20, null=True, default=None)

    def __str__(self):
        return f"{self.group} -> BH: {self.button_height} ; " + \
               f"BTXT: {self.button_txt}"


# </editor-fold desc="DISPLAY">
