from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.staticfiles.views import serve
from base.admin import ScheduledCourseResource
from base.models import ScheduledCourse

# Create your views here.
def hello_world(request):
    return HttpResponse("Salut !")


def display(request, year):
    if year is None:
        year = 0

    try:
        year = int(year)
    except ValueError:
        year = 0

    return render(request,
                  'base/edt.html',
                  {'yyyy': year})


def fetch_courses(request, year):

    if year not in [2018, 2019]:
        return serve(request,
                     'base/data.csv')

    return serve(request,
                 'base/data' + str(year) + '.csv')


def fetch_scheduled_courses(req, year=None, week=None):
    if year is None or week is None:
        year = 2018
        week = 10
    try:
        year = int(year)
        week = int(week)
    except ValueError:
        return HttpResponse("KO")
    dataset = None
    dataset = ScheduledCourseResource().export(ScheduledCourse.objects.filter(course__week=week,course__year=year))
    if dataset is None:
        raise Http404("What are you trying to do?")
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['week'] = week
    response['year'] = year
    return response
