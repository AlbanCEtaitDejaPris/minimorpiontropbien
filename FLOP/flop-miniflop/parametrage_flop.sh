#!/bin/bash

add-apt-repository ppa:jonathonf/python-3.6

apt-get update

apt-get --assume-yes install python3.6

update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1

update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2

apt-get --assume-yes install python3.6-dev

apt-get --assume-yes install python3-pip

apt-get --assume-yes install git-all

apt-get --assume-yes install postgresql

mkdir /sysinfo/

cd /sysinfo/

wget https://framagit.org/flopedt/FlOpEDT/raw/dev/requirements.txt

python3 -m pip install --no-cache-dir -r requirements.txt
