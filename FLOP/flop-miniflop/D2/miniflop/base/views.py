from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.staticfiles.views import serve

# Create your views here.
def hello_world(request):
    return HttpResponse("Hello World !")

def display(request,year='0000'):
    return render(request,'base/edt.html',{'year':str(year),})

def fetch_scheduled_courses(request,year):
    return serve(request,'base/data'+str(year)+'.csv')
