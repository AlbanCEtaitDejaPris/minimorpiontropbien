from django.shortcuts import render

# Create your views here.
def fetch_tutors(request):
    dataset = None
    dataset = TutorResource().export(Tutor.objects.all())
    if dataset is None:
        raise Http404("What are you trying to do?")
    response = HttpResponse(dataset.csv, content_type='text/csv')
    return response
