/***************/
/* static grid */
/***************/
/*
   ______ (0,0)
  |
  v                         x
  o-----+-----+-----+------->
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     |     |     |
  |     |     |     |
  +-----+-----+-----+
  |     <----->
  |    len_square
  |
  v
y

*/
//init de la grille
var len_square =100;
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", len_square)
    .attr("y1", 0*len_square)
    .attr("x2", len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 2*len_square)
    .attr("y1", 0*len_square)
    .attr("x2", 2*len_square)
    .attr("y2", 3*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0*len_square)
    .attr("y1", 1*len_square)
    .attr("x2", 3*len_square)
    .attr("y2", 1*len_square);
d3.select("svg#grille")
    .append("line")
    .attr("stroke", "black")
    .attr("stroke-width", 2)
    .attr("x1", 0*len_square)
    .attr("y1", 2*len_square)
    .attr("x2", 3*len_square)
    .attr("y2", 2*len_square);
//-----------------------------------------
// le jeu
var radius =40;
var defaultColor ="#e888ee";
var game =new Object();
game.players =[{"name":"FP","color":"#00FFFF"},
               {"name":"RB","color":"#FFFF00"}];
var players =new Map();
for (var i = 0; i < game.players.length; i++) {
  players.set(game.players[i].name,game.players[i]);
}
function move_x(x){
  return x*len_square+len_square/2
}
function move_y(y){
  return y*len_square+len_square/2
}
function move_color(j){
  if (players.has(j)) {
      return players.get(j).color;
  }else {
    return defaultColor;
  }
}

d3.select("svg#grille")
    .append("circle")
    .attr("cx",move_x(2))
    .attr("cy",move_y(1))
    .attr("r", 40)
    .attr("fill", move_color("FP"));
console.log("Tout va bien ! ");
