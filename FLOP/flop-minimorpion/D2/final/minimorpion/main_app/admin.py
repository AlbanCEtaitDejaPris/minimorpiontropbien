from django.contrib import admin
from .models import Player
# Register your models here.
class PlayerAdmin(admin.ModelAdmin):
    """docstring for PlayerAdmin."""
    list_display=('username','color')

admin.site.register(Player,PlayerAdmin)
