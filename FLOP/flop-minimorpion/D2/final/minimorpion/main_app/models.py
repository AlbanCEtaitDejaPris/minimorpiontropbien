from django.db import models
from colorfield.fields import ColorField
from django.contrib.auth.models import User
# Create your models here.

class Player(User):
    """docstring for Player."""
    color= ColorField(default='#FFFF00')
class Ranking(models.Model):
    player = models.OneToOneField('Player',on_delete=models.CASCADE)
    victories = models.PositiveIntegerField(default=0)
    defeats  = models.PositiveIntegerField(default=0)
    ties  = models.PositiveIntegerField(default=0)

class LastGame(models.Model):
    player1 = models.ForeignKey('Player',on_delete=models.CASCADE,related_name='game_first')
    player2 = models.ForeignKey('Player',on_delete=models.CASCADE,related_name='game_second')
    cell_line = models.PositiveIntegerField(default=0)
    cell_column = models.PositiveIntegerField(default=0)
    cell_player =models.ForeignKey('Player',on_delete=models.CASCADE,related_name='game_cells')
