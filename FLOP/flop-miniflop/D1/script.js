/*------------------------
  ------ VARIABLES -------
  ------------------------*/

// input
var days = [{num: 0, ref: "m", name: "Lun."},
            {num: 1, ref: "tu", name: "Mar."},
            {num: 2, ref: "w", name: "Mer."},
            {num: 3, ref: "th", name: "Jeu."},
            {num: 4, ref: "f", name: "Ven."}] ;
var idays = {};
var groups = {} ;
var day_start = 8*60 ;      //8*60 ;
var day_end = 18*60+45 ;    //18*60 + 45 ;


// data
var courses = [] ;

// display settings
var min_to_px = 1 ;
var gp = {width: 30, nb_max: 7} ;


// initialisation function
function init() {

    d3.select("svg")
        .attr("height", svg_height())
        .attr("width", svg_width()) ;

    for(var i = 0 ; i<days.length ; i++){
        idays[days[i].ref] = days[i].num ;
    }

    groups["CE"] = {start: 0, width: 6} ;
    groups["1"] =  {start: 0, width: 2} ;
    groups["1A"] = {start: 0, width: 1} ;
    groups["1B"] = {start: 1, width: 1} ;
    groups["2"] =  {start: 2, width: 2} ;
    groups["2A"] = {start: 2, width: 1} ;
    groups["2B"] = {start: 3, width: 1} ;
    groups["3"] =  {start: 4, width: 2} ;
    groups["3A"] = {start: 4, width: 1} ;
    groups["3B"] = {start: 5, width: 1} ;
    groups["4"] =  {start: 6, width: 1} ;
    groups["234"]= {start: 2, width: 5} ;
}

// mes truc pratiques
const dayWidth =gp.width*gp.nb_max;
const dayHeigth =day_end-day_start;
const dayY=0;

function dayX(day){
  return idays[day]*dayWidth;
}


/*------------------------
  ---- READ DATA FILE ----
  ------------------------*/

function fetch_courses() {

    $.ajax({
        type: "GET",
        dataType: 'text',
        url: "./data.csv",
        async: true,
        contentType: "text/csv",
        success: function(msg, ts, req) {
            console.log(msg);
            courses=d3.csvParse(msg);
            console.log(courses);
            courses_display();

        },
        error: function(msg) {
            console.log("Error while reading the courses.");
        }
    });

}

function courses_display(){
  for (var i = 0; i < courses.length; i++) {
    d3.select("svg")
    .append("rect")
    .attr("x",dayX(courses[i].day)+ groups[courses[i].gp_name].start*gp.width)
    .attr("y",courses[i].start_time-day_start)
    .attr("width",groups[courses[i].gp_name].width*gp.width)
    .attr("height",courses[i].duration)
    .attr("fill",courses[i].color_bg);
  }
}


function svg_height() {
    return (day_end - day_start) * min_to_px + 200 ;
}
function svg_width() {
    return days.length * gp.nb_max * gp.width ;
}
/*------------------
  ------ RUN -------
  ------------------*/

init();
fetch_courses() ;
