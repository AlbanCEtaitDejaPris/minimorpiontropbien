from django.shortcuts import render
from django.http import HttpResponse
import json
from .utils import Game

# Create your views here.
def hello(request):
    return HttpResponse("Hé Alban, regarde moi ça marche ! \n Toi t'as pleins d'erreurs mdr")

def play(request):
    return render(request,'main_app/morpion.html',{})

def save(request):
    game=Game(json.loads(request.POST.get('game',{})))
    game.print_game()
    return HttpResponse(status = 204)
